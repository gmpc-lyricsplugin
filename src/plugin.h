#ifndef __LYRICS_PLUGIN__
#define __LYRICS_PLUGIN__
typedef struct _LyricsPluginPrivate LyricsPluginPrivate;

typedef struct _LyricsPlugin
{
        GmpcPluginBase  parent_instance;
        LyricsPluginPrivate *priv;
} LyricsPlugin ;

typedef struct _LyricsPluginClass
{
        GmpcPluginBaseClass parent_class;
} LyricsPluginClass;


#endif
