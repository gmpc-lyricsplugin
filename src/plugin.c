/* gmpc-lyricsplugin (GMPC plugin)
 * Copyright (C) 2008-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <config.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <gmpc/gmpc_easy_download.h>
#include <gmpc/plugin.h>
#include <gmpc/gmpc-extras.h>
#include "plugin.h"

#define LYRIC_PLUGINs_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), lyricsplugin_plugin_get_type(), LyricsPlugin))

const GType lyricsplugin_plugin_get_type(void);
/**
 * Get/Set enabled 
 */
static int lyricsplugin_get_enabled(GmpcPluginBase *base)
{
	return cfg_get_single_value_as_int_with_default(config, "lyricsplugin-plugin", "enable", TRUE);
}
static void lyricsplugin_set_enabled(GmpcPluginBase *base,int enabled)
{
	cfg_set_single_value_as_int(config, "lyricsplugin-plugin", "enable", enabled);
}

/* Get priority */
static int lyricsplugin_fetch_cover_priority(GmpcPluginMetaDataIface *base){
	return cfg_get_single_value_as_int_with_default(config, "lyricsplugin-plugin", "priority", 80);
}
static void lyricsplugin_fetch_cover_priority_set(GmpcPluginMetaDataIface *base, int priority){
	cfg_set_single_value_as_int(config, "lyricsplugin-plugin", "priority", priority);
}
typedef struct Query {
    mpd_Song *song;
    void (*callback)(GList *, gpointer);
    gpointer user_data;
}Query;

static void lyricsplugin_download_callback(const GEADAsyncHandler *handle, GEADStatus status, gpointer data)
{
    Query *q = (Query *)data;
    GList *list = NULL;
    if(status == GEAD_PROGRESS) return;
    if(status == GEAD_DONE) 
    {
        goffset size= 0;
        const char *data = gmpc_easy_handler_get_data(handle, &size);
        xmlDocPtr doc = xmlReadMemory(data, (int)size, gmpc_easy_handler_get_uri(handle), NULL, XML_PARSE_NOERROR|XML_PARSE_RECOVER);//xmlParseMemory(data,(int)size);
        if(doc)
        {
            xmlNodePtr root = xmlDocGetRootElement(doc);
            xmlNodePtr cur = root->xmlChildrenNode;
            for(;cur;cur = cur->next){
                if(xmlStrEqual(cur->name, (xmlChar *)"body")){
                    xmlNodePtr cur2 = cur->xmlChildrenNode;
                    for(;cur2;cur2 = cur2->next)
                    {
                        if(xmlStrEqual(cur2->name , (xmlChar *)"div"))
                        {
                            xmlChar *property = xmlGetProp(cur2, "id");
                            if(property && xmlStrEqual(property, "lyrics"))
                            {
                                xmlChar *lyric = xmlNodeGetContent(cur2);
                                if(lyric && strlen(lyric) > 10)
                                {
                                    MetaData *mtd = meta_data_new();
                                    mtd->type = META_SONG_TXT;
                                    mtd->plugin_name = _("Lyrics Plugin");
                                    mtd->content_type = META_DATA_CONTENT_TEXT;
                                    mtd->content = g_strdup(g_strstrip(lyric));
                                    mtd->size = -1;
                                    list = g_list_append(list,mtd); 
                                }
                                xmlFree(lyric);
                            }
                            if(property)xmlFree(property);
                        }
                    }
                }
            }
            xmlFreeDoc(doc);
        }
    }

    q->callback(list, q->user_data);
    g_free(q);
}

static void lyricsplugin_get_uri(GmpcPluginMetaDataIface *base, const mpd_Song *song, MetaDataType type, GmpcPluginMetaDataCallback callback, void *user_data)
{
    g_debug("Lyricwiki plugin api V2");
    if(lyricsplugin_get_enabled(GMPC_PLUGIN_BASE(base)) && type == META_SONG_TXT && song && song->artist &&song->title)
    {
        Query *q = g_malloc0(sizeof(*q));
        gchar *artist = gmpc_easy_download_uri_escape(song->artist);
        gchar *title =  gmpc_easy_download_uri_escape(song->title);

        gchar *uri_path = g_strdup_printf(
                "http://www.lyricsplugin.com/winamp03/plugin/?artist=%s&title=%s",
                artist, title);
        q->callback = callback;
        q->song = (mpd_Song *) song;
        q->user_data = user_data;

        g_free(artist); g_free(title);
        g_debug("Trying: '%s'", uri_path);
        if(gmpc_easy_async_downloader(uri_path, lyricsplugin_download_callback, q)!= NULL)
        {
            g_free(uri_path);
            return;
        }
        g_free(q);
        g_free(uri_path);
    }
    /* Return nothing found */
    callback(NULL, user_data);
}

/**
 * Gobject plugin
 */
static void lyricsplugin_plugin_class_init (LyricsPluginClass *klass);

static int *lyricsplugin_plugin_get_version(GmpcPluginBase *plug, int *length)
{
	static int version[3] = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION};
	if(length) *length = 3;
	return (int *)version;
}

static const char *lyricsplugin_plugin_get_name(GmpcPluginBase *plug)
{
	return _("Lyrics Plugin");
}
static GObject *lyricsplugin_plugin_constructor(GType type, guint n_construct_properties, GObjectConstructParam * construct_properties) {
	LyricsPluginClass * klass;
	LyricsPlugin *self;
	GObjectClass * parent_class;
	klass = (g_type_class_peek (lyricsplugin_plugin_get_type()));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	self = (LyricsPlugin *) parent_class->constructor (type, n_construct_properties, construct_properties);


    /* Setup textdomain */
	bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");

    GMPC_PLUGIN_BASE(self)->translation_domain = GETTEXT_PACKAGE;
	GMPC_PLUGIN_BASE(self)->plugin_type = GMPC_PLUGIN_META_DATA;

	return G_OBJECT(self);
}
static void lyricsplugin_plugin_finalize(GObject *obj) {
    LyricsPlugin *self = (LyricsPlugin *)obj;
	LyricsPluginClass * klass = (g_type_class_peek (play_queue_plugin_get_type()));
	gpointer parent_class = g_type_class_peek_parent (klass);

    if(parent_class)
        G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void lyricsplugin_plugin_class_init (LyricsPluginClass *klass)
{
	G_OBJECT_CLASS(klass)->finalize =		lyricsplugin_plugin_finalize;
	G_OBJECT_CLASS(klass)->constructor =	lyricsplugin_plugin_constructor;
	/* Connect plugin functions */
	GMPC_PLUGIN_BASE_CLASS(klass)->get_version = lyricsplugin_plugin_get_version;
	GMPC_PLUGIN_BASE_CLASS(klass)->get_name =	 lyricsplugin_plugin_get_name;

	GMPC_PLUGIN_BASE_CLASS(klass)->get_enabled = lyricsplugin_get_enabled;
	GMPC_PLUGIN_BASE_CLASS(klass)->set_enabled = lyricsplugin_set_enabled;

}

static void lyricsplugin_plugin_meta_data_iface_init(GmpcPluginMetaDataIfaceIface * iface) {
    (iface)->get_priority = lyricsplugin_fetch_cover_priority;
    (iface)->set_priority = lyricsplugin_fetch_cover_priority_set;
    (iface)->get_metadata = lyricsplugin_get_uri;
}

const GType lyricsplugin_plugin_get_type(void) {
	static GType lyricsplugin_plugin_type_id = 0;
	if(lyricsplugin_plugin_type_id == 0) {
		static const GTypeInfo info = {
			.class_size = sizeof(LyricsPluginClass),
			.class_init = (GClassInitFunc)lyricsplugin_plugin_class_init,
			.instance_size = sizeof(LyricsPlugin),
			.n_preallocs = 0
		};

		lyricsplugin_plugin_type_id = g_type_register_static(GMPC_PLUGIN_TYPE_BASE, "LyricsPlugin", &info, 0);

        /** Browser interface */
		static const GInterfaceInfo iface_info = { (GInterfaceInitFunc) lyricsplugin_plugin_meta_data_iface_init, 
            (GInterfaceFinalizeFunc) NULL, NULL};
		g_type_add_interface_static (lyricsplugin_plugin_type_id, GMPC_PLUGIN_TYPE_META_DATA_IFACE, &iface_info);
	}
	return lyricsplugin_plugin_type_id;
}

GType plugin_get_type(void);

G_MODULE_EXPORT GType plugin_get_type(void)
{
    return lyricsplugin_plugin_get_type();
}
